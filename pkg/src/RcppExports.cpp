// Generated by using Rcpp::compileAttributes() -> do not edit by hand
// Generator token: 10BE3573-1514-4C36-9D1C-5A225CD40393

#include <Rcpp.h>

using namespace Rcpp;

// getSuffStats_SIR
Rcpp::NumericVector getSuffStats_SIR(const Rcpp::NumericMatrix& pop_mat, const int ind_final_config);
RcppExport SEXP _pkg_getSuffStats_SIR(SEXP pop_matSEXP, SEXP ind_final_configSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< const Rcpp::NumericMatrix& >::type pop_mat(pop_matSEXP);
    Rcpp::traits::input_parameter< const int >::type ind_final_config(ind_final_configSEXP);
    rcpp_result_gen = Rcpp::wrap(getSuffStats_SIR(pop_mat, ind_final_config));
    return rcpp_result_gen;
END_RCPP
}

static const R_CallMethodDef CallEntries[] = {
    {"_pkg_getSuffStats_SIR", (DL_FUNC) &_pkg_getSuffStats_SIR, 2},
    {NULL, NULL, 0}
};

RcppExport void R_init_pkg(DllInfo *dll) {
    R_registerRoutines(dll, NULL, CallEntries, NULL, NULL);
    R_useDynamicSymbols(dll, FALSE);
}
