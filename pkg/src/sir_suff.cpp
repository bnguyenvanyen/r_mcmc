#include <Rcpp.h>

using namespace Rcpp;


// [[Rcpp::export]]
Rcpp::NumericVector getSuffStats_SIR(const Rcpp::NumericMatrix& pop_mat, const int ind_final_config) {
  // initialize sufficient statistics
  int num_inf = 0;       // number of infection events
  int num_rec = 0;       // number of recovery events
  double beta_suff = 0;  // integrated hazard for the infectivity
  double nu_suff = 0;    // integrated hazard for the recovery
  // initialize times
  double cur_time = 0;              // current time
  double next_time = pop_mat(0,0);  // time of the first event
  double dt = 0;                    // time increment
  
  // compute the sufficient statistics - loop through the pop_mat matrix until
  // reaching the row for the final observation time
  for(int j = 0; j < ind_final_config - 1; ++j) {
  
            cur_time = next_time;         
            next_time = pop_mat(j+1, 0); // grab the time of the next event
            dt = next_time - cur_time;   // compute the time increment
            
            beta_suff += pop_mat(j, 3) * pop_mat(j, 4) * dt; // add S*I*(t_{j+1} - t_j) to beta_suff
            nu_suff += pop_mat(j, 4) * dt;                   // add I*(t_{j+1} - t_j) to nu_suff
            
            // increment the count for the next event
            if(pop_mat(j + 1, 2) == 1) {  
                      num_inf += 1;
            } else if(pop_mat(j + 1, 2) == 2) {
                      num_rec += 1;
            }
  }
          
  // return the vector of sufficient statistics for the rate parameters
  return Rcpp::NumericVector::create(num_inf, beta_suff, num_rec, nu_suff);
}

